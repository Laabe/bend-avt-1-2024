// №1
function getList() {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        // 1.3
        // reject(new Error("Не удалось получить список задач")); 
  
        resolve([
          { id: 1, title: 'Task 1', isDone: false },
          { id: 2, title: 'Task 2', isDone: true },
        ]); 
      }, 2000);
      
    });
  }
  
  // 1.2
  getList()
  .then(data => {
    data.forEach(task => {
      console.log(`Task ${task.id}: ${task.title} - ${task.isDone ? 'Готово' : 'Не готово'}`);
    });
  })
    // 1.4
    .catch(error => {
      console.error("Произошла ошибка:", error);
     
    });
// _____________________________________________________________________________________________________________________________________
// №2
function createPromiseChain() {
  const words = ["Я", "использую", "цепочки", "обещаний"];
  let promise = Promise.resolve("");

  words.forEach((word, index) => {
      promise = promise.then(result => {
          return new Promise(resolve => {
              setTimeout(() => {
                  resolve(`${result} ${word}`);
              }, (index + 1) * 1000);
          });
      });
  });

  promise.then(finalResult => {
      console.log(finalResult.trim());
  });
}

createPromiseChain();
// __________________________________________________________________________________________________________________________________________
// №3
function createParallelPromises() {
  const words = {
      "Я": 1000,
      "использую": 800,
      "вызов": 1200,
      "обещаний": 700,
      "параллельно": 500
  };
  
  const promises = Object.entries(words).map(([word, delay]) => {
      return new Promise(resolve => {
          setTimeout(() => {
              resolve(word);
          }, delay);
      });
  });

  Promise.all(promises)
      .then(results => {
          const finalResult = results.join(' ');
          console.log(finalResult);
      });
}

createParallelPromises();
// ___________________________________________________________________________________________________________________________________________
// №4
function delay(ms) {
  return new Promise(resolve => {
      setTimeout(resolve, ms);
  });
}

delay(2000).then(() => {
  console.log('Это сообщение вывелось через 2 секунды');
});
// ______________________________________________________________________________________________________________________________________________
// №5
function delay(ms) {
  return new Promise(resolve => {
      setTimeout(resolve, ms);
  });
}

async function createDelays() {
  const words = ["Я", "использую", "вызов", "обещаний", "параллельно"];

  let finalString = '';

  for (let i = 0; i < words.length; i++) {
      const word = words[i];
      const delayTime = [1000, 800, 1200, 700, 500][i];
      await delay(delayTime);
      finalString += word + ' ';
  }

  console.log(finalString.trim());
}

createDelays();
// ________________________________________________________________________________________________________________________________________________
// №6
async function filmWithTatooine() {
    const planetResponse = await fetch('https://swapi.dev/api/planets/?search=Tatooine');
    const planetData = await planetResponse.json();

    if (planetData.results.length === 0) {
        console.log('Планета Татуин не найдена.');
        return;
    }

    const tatooineUrl = planetData.results[0].films[0];
    const filmResponse = await fetch(tatooineUrl);
    const filmData = await filmResponse.json();

    console.log('Название фильма:', filmData.title);
    console.log('Год выпуска:', filmData.release_date);
    console.log('Режиссер:', filmData.director);
}

filmWithTatooine();
// _________________________________________________________________________________________________________________________________________________
// №7
const fetchVehicleName = async () => {
  const response = await fetch('https://swapi.dev/api/people/11/');
  const data = await response.json();

  const vehiclesUrls = data.vehicles;
  const firstVehicleResponse = await fetch(vehiclesUrls[0]);
  const firstVehicleData = await firstVehicleResponse.json();

  console.log('Название транспортного средства, на котором Anakin Skywalker впервые ехал:', firstVehicleData.name);
};

fetchVehicleName();
// _________________________________________________________________________________________________________________________________________________
// №8
const payload = { message: "Привет сервис, я жду от тебя ответа" };

fetch('http://localhost:3000/echo', {
    method: 'POST',
    headers: {
        'Content-Type': 'application/json',
    },
    body: JSON.stringify(payload),
})
.then(response => response.json())
.then(data => {
    console.log('Ответ от сервера:', data);
})
.catch(error => {
    console.error('Произошла ошибка:', error);
});